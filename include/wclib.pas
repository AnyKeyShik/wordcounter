unit wclib;

interface
    uses consts, list;

    procedure read_text(var words : long_text);
    procedure sort_text(var words : long_text);
    procedure write_text(words : long_text);
    procedure delete_text(var words : long_text);

implementation

    {
    Delete duplicates words in list
        words : list for find duplicates
    }
    procedure delete_dups(var words : long_text);
    var
        tmp, traverse : long_text;
    begin

        traverse := words;

        while traverse^.next <> nil do
        begin
            tmp := traverse^.next;
            
            if(traverse^.word = tmp^.word) then
            begin
                traverse^.count := traverse^.count + 1;
                traverse^.next := traverse^.next^.next;
                dispose(tmp);
            end
            else
                traverse := traverse^.next;
        end;
    end;

    {
    Clear word after input
        word: list for clear
    }
    procedure clear_array(var word : _word);
    begin
        word := default(_word);
    end;

    {
    Read words
        words: list for insert
    }
    procedure read_text(var words : long_text);
    var 
        c : char;
        word : _word; 
        i : integer;
        node : long_text;
    
    begin
        clear_array(word);
        i := 1;

        repeat
            read(c);
            if (c <> ',') and (c <> '.') then begin
                word[i] := c;
                i := i + 1;
            end else if (c = ',') or (c = '.') then begin
                node := create_node(word, i - 1);
                add_last(node);

                i := 1;
                clear_array(word);
            end;
        until c = '.';
      
        create_list(words);
    end;

    {
    Sort text in the list
        words: text for sorting
    }
    procedure sort_text(var words : long_text);
    var
        start, traverse, min : long_text;
    begin
        start := words;
        
        while start^.next <> nil do
        begin
            min := start;
            traverse := start^.next;

            while traverse <> nil do
            begin
                if(min^.word > traverse^.word) then
                begin
                    min := traverse;
                end;

                traverse := traverse^.next;
            end;

            swap_words(start, min);

            write('.');

            start := start^.next;
        end; 

        delete_dups(words);
    end;

    {
    Output list of the words
        words: list for output
    }
    procedure write_text(words : long_text);
    begin
        while words <> nil do
        begin
            write(words^.word:words^.len);
            writeln(' - ', words^.count, ' ');
            words := words^.next;
        end;
    end;

    {
    Delete list
        words: list for delete
    }
    procedure delete_text(var words : long_text);
    var 
        tmp : long_text;
    begin
        while words <> nil do
        begin
            tmp := words;
            words := words^.next;
            dispose(tmp);

            write('.');
        end;
    end;
end.
