unit consts;

interface
    const 
        WORD_LEN = 6;
        SEP_LEN  = 35;

    type 
        _word = packed array[1..WORD_LEN] of char;
        long_text = ^node;
        node = record
            word : packed array[1..WORD_LEN] of char;
            count : integer;
            len : integer;
            next : long_text
    end;
   
implementation
end.
