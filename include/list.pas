unit list;

interface
    uses consts;

    procedure list_init(words : long_text);
    procedure create_list(var words : long_text);
    function create_node(new_word : _word; len : integer) : long_text;
    procedure add_last(new_node : long_text);
    procedure add_after(var node : long_text; new_node : long_text);
    procedure swap_words(word1 : long_text; word2 : long_text);

implementation
    var 
        head : long_text;

    {
    Set `head` var to list start
        words: pointer to the list
    }
    procedure list_init(words : long_text);
    begin
        head := words;
    end;

    procedure create_list(var words : long_text);
    begin
        words := head;
    end;

    {
    Create node for insert into the list. 
        new_word: word for new node
        len: len of the word
    }
    function create_node(new_word : _word; len : integer) : long_text;
	var 
		new_node : long_text;
	begin
  		New(new_node);
  		
		new_node^.word := new_word;
  		new_node^.count := 1;
        new_node^.len := len;
 	 	new_node^.next := nil;
  		
		create_node := new_node;
    end;

    {
    Add first element to the list
        new_node: element for insert
    }
    procedure add_first(new_node : long_text);
    begin
        new_node^.next := head;
        head := new_node;
    end;

    {
    Add element to the list after another element
        node:       the element after which the new one will be inserted
        new_node:   element for insert
    }
    procedure add_after(var node : long_text; new_node : long_text);
    begin
        new_node^.next := node^.next;
        node^.next := new_node;
    end;
    
    {
    Add element to the end of the list
        new_node: element for insert
    }
	procedure add_last(new_node : long_text);
    var 
        pp: long_text;
    
    begin
        if head = nil then
            add_first(new_node)
        else begin
            pp := head;
                
            while pp^.next <> nil do 
                pp := pp^.next;
            
            add_after(pp, new_node);
		end;
	end;

    {
    Swap two elements in the list
        word1, word2: words for swap
    }
    procedure swap_words(word1 : long_text; word2 : long_text);
    var
        tmp : _word;
    begin
        tmp := word1^.word;
        word1^.word := word2^.word;
        word2^.word := tmp;
    end;
end. 
