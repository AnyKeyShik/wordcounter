Program WordCounter;

uses crt, wclib, consts, list;

var
    i : integer;
    words : long_text;

begin
    clrscr;

    writeln('Welcome to the World Word Counter!');
    writeln('Initialization started please wait...');
    list_init(words);

    writeln('Please enter your words sequence separated by commas and ends by dot');
    write('> ');
    read_text(words);
   
    for i := 1 to SEP_LEN do
        write('-');
    writeln();

    writeln('Sorting...');
    writeln('Starting');
    write('Progress: [');
    sort_text(words);
    writeln(']');
    writeln('Done!');

    for i := 1 to SEP_LEN do
        write('-');
    writeln();

    writeln('Your sorted words look like this:');
    write_text(words);

    writeln();
    for i := 1 to SEP_LEN do 
        write('-');
    writeln();

    writeln('Memory release...');
    writeln('Starting');
    write('Progress: [');
    delete_text(words);
    writeln(']');
    writeln('Done!');

    readln();
end.
